package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/dhowden/tag"
	"gitlab.com/diamondburned/qprompt"
)

/*
	TODO: `ffmpeg -i testsong.ogg -map_metadata 0:s:0 -write_id3v2 1 -metadata "ARTIST=Mariya Takeuchi" testsong.ogg`
		1. Read a directory, print ALL the artist names ONLY IF KANA RETURNS KANJI == TRUE
		2. Have prompt to edit
		3. Override with ffmpeg cmd above
		4. Keep going
*/

func main() {
	qprompt.ListSize = 10
	filename := strings.Join(os.Args[1:], " ")

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal("Error while opening mp3 file:", err)
	}

	defer f.Close()

	m, err := tag.ReadFrom(f)
	if err != nil {
		log.Fatal("Error when reading tags:", err)
	}

	table := convertMapSlice(m.Raw())
	fmt.Println(tabPrinter(table))

	_, key, err := qprompt.SelectStrings(
		"Metadata to change",
		append([]string{"Skip"}, getMetadataKeys(table)...),
	)

	if err != nil {
		log.Fatalln(err)
	}

	if key == "Skip" {
		return
	}

	new, err := qprompt.Prompt(key, "", nil)
	if err != nil {
		panic(err)
	}

	if new == "" {
		log.Println("Not changed")
	}

	if err := ChangeArtist(f.Name(), [][2]string{
		[2]string{key, new},
	}); err != nil {
		panic(err)
	}
}
