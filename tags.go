package main

import (
	"fmt"
	"sort"
	"strings"
	"text/tabwriter"
)

func tabPrinter(table [][2]string) string {
	var s strings.Builder
	var w = tabwriter.NewWriter(&s, 4, 4, 1, ' ', 0)
	for _, row := range table {
		fmt.Fprintf(w, "%s\t%s\n", row[0], row[1])
	}

	w.Flush()
	return s.String()
}

func getMetadataKeys(table [][2]string) []string {
	ks := make([]string, len(table))
	for i := range ks {
		ks[i] = table[i][0]
	}
	return ks
}

func convertMapSlice(m map[string]interface{}) [][2]string {
	array := make([][2]string, 0, len(m))
	for k, v := range m {
		array = append(array, [2]string{k, fmt.Sprint(v)})
	}

	sort.Slice(array, func(i, j int) bool {
		return array[i][0] < array[j][0]
	})

	return array
}
